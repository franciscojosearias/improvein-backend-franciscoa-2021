# improvein-backend-franciscoa-2021

## 1. [How to run the app](#id1)
### 1.1 [dependencies](#id11)
### 1.2 [devDependencies](#id12)
## 2. [How to works the app](#id2)
## 2.1 [interacting with the app](#id21)
### 2.1.1 [Routes](#id211)

```

```


<div id='id1' > <h1><b> 1. How to run the app </b></h1> </div>

<b>first</b>

go to the path directory where you want to clone the repository and exec on your git bash: git clone <:url>

<b>second</b>

once you've cloned the repository, go to the path directory where you've cloned it and exec on node js command prompt: 
npm install.

<b>finally</b>

exec on the path directory where you've cloned the repository following command: npm run dev


<div id='id11'><h2>1.1 dependencies</h2></div>

General dependencies, to watch them, please open package.json with some special text editor like note pad ++ and watch it.


<div id='id12'><h2>1.2 devDependencies</h2></div>

dependencies of first, second or down levels, to watch them, please open package.json with some special text editor like note pad ++ and watch it.


<div id='id2'><h1><b>2. How to works the app</b></h1></div>

The app works with middlewares to register and authenticates user. When the user register in application, he receives e token. And same when he signs in. That token expires in 24 hs.

The user can have 1 role of all roles. The roles are three. Those are: "user"; He only can use GET methods, "admin": He can use GET methods and add and delete objects. And by last,"moderator": He can use Get methods to fetch objects and add and update objects.

To be able creating, updating and deleting objects using the CRUD paradigm, it's enough the user has these roles: "admin" and "moderator".




<div id='id21'><h2>2.1 interacting with the app</h2></div>

Once the user signed up and signed in using POST method inserting the JSON object, he will have got a token once he signed in.

*He will must copy the token received once signed in and in PUT or POST request, go to body, select "raw" - "application/JSON" or "JSON", then go to headers at the same section, in key type: "x-api-key" value "application/json" and key below type: "x-access-token" value: (*copy the token that received once he signed in and paste here). Then, he will be able to add and put objects.

ONCE the token expires in 24 hours, he will have to generating another one, must signing in the api rest.


<div id='id211'><h3>2.1.1 routes:</h3>

<h2><b>Users:</b></h2>

<h3>methods:</h3> 

```
Only has one post method and is:

     http://localhost:4000/api/users


```
<h2><b>Auth:</b></h2>


<h3>methods:</h3>

```
Only has two post methods:

    signup:

    http://localhost:4000/api/auth/signup?

    signin:

    http://localhost:4000/api/auth/signin



 ```

 <h2><b>Actors:</b></h2>


<h3>GET Methods:</h3>

```
    FETCH ALL actors:

       http://localhost:4000/api/actors

    FETCH actor by id:

         http://localhost:4000/api/actors/<:actorId>

    FETCH actors by nationality

        http://localhost:4000/api/actors/actorsask/ask?nationality=<"insert value">



```


<h3>CRUD Methods:</h3>

```

    POST:

        http://localhost:4000/api/actors

    PUT:

       http://localhost:4000/api/actors/<:actorId>

    DELETE:

       http://localhost:4000/api/actors/<:actorId>



```



<h2><b>Directors:</b></h2>

<h3>GET methods:</h3>

```
      FETCH all directors:

          http://localhost:4000/api/directors

      FETCH director by id:

 http://localhost:4000/api/directors/<:directorId>

      FETCH directors by nationality:

http://localhost:4000/api/  directors/directorsask/ask?nationality=<"insert value">



```

<h3>CRUD Methods:</h3>

```

      POST:

        http://localhost:4000/api/directors

      PUT:

        http://localhost:4000/api/directors/<:directorId>

      DELETE:

        http://localhost:4000/api/directors/<:directorId>

```



<h2><b>TV Shows:</b></h2>


<h3>GET methods:</h3>

```

        FETCH all tv shows:

          http://localhost:4000/api/tvshows

        FETCH tv show by id:

        http://localhost:4000/api/tvshows/<:tvShowId>

        FETCH specific episode from tv show:

        http://localhost:4000/api/tvshows/showid/<:tvShowId>/seasonid/<:seasonId>/episodeid/<:episodeId>


```


<h3>CRUD Methods:</h3>

```

          POST:

            http://localhost:4000/api/tvshows

          PUT:

          http://localhost:4000/api/tvshows/<:tvShowId>


          DELETE:

         http://localhost:4000/api/tvshows/<:tvShowId>

```


<h2><b>Movies:</b></h2>


<h3>GET methods:</h3>

```
        FETCH all movies:

        http://localhost:4000/api/movies

        FETCH movie by id:

   http://localhost:4000/api/movies/<:movieId>

        FETCH specific movies sorted by year and filtered by country:

 http://localhost:4000/api/movies/movieask/asking?country=<"insert value">

```

<h3>CRUD Methods:</h3>

```
        POST:

        http://localhost:4000/api/movies

        PUT:

        http://localhost:4000/api/movies/<:movieId>

        DELETE:

        http://localhost:4000/api/movies/<:movieId>
    


```


<b>NOTE: </b> When the users are using postman and type the route passing parameter, in the routes with parameters or query, must replace instead of <:idParameter> or ?attribute=<"inserting value"> by the value 
they want.

</div>





