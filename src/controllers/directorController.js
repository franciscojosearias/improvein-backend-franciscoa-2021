import Director from "../models/Director";


//GET METHODS 

export const getDirectors = async (req, res) => {
  const directors = await Director.find();
  return res.json(directors);
};


export const getDirectorById = async (req, res) => {
  const { directorId } = req.params;

  const director = await Director.findById(directorId);
  res.status(200).json(director);
};

export const getDirectorByNationality = async (req,res) =>{

  try{


    const findQuer = req.query.nationality ? {nationality: req.query.nationality} : {}


  const directors = await Director.find(findQuer).sort({fullName: 1});

  return res.json(directors);


  }catch (e) {
    res.status(500).send({ mensaje: e });
  }
};



//CRUD METHODS

export const createDirector = async (req, res) => {
  const { fullName, nationality } = req.body;

  try {
    const newDirector = new Director({
      fullName,
      nationality
    });

    const directorSaved = await newDirector.save();

    res.status(201).json(directorSaved);
  } catch (error) {
    return res.status(500).json(error);
  }
};


export const updateDirectorById = async (req, res) => {
  const updatedDirector = await Director.findByIdAndUpdate(
    req.params.directorId,
    req.body,
    {
      new: true,
    }
  );
  res.status(200).json(updatedDirector);
};

export const deleteDirectorById = async (req, res) => {
  const { directorId } = req.params;

  await Director.findByIdAndDelete(directorId);

 
  res.status(200).json();
};