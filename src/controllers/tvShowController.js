import TVShow from "../models/TVShow";


import { Types } from "mongoose";


//GET METHODS

  
export const getTvShows = async (req, res) => {
    const tvShows = await TVShow.find()
    .populate({
      path: 'seasons.episodes.cast'
    })
    .populate({
      path: 'seasons.episodes.director'
    });
    return res.json(tvShows);
  };


export const getTvShowById = async (req, res) => {
    const { tvShowId } = req.params;
  
    const tvShow = await TVShow.findById(tvShowId)
    .populate({
      path:'seasons.episodes.cast'
    })
    .populate({
      path:'seasons.episodes.director'
    });
    res.status(200).json(tvShow);
  };

  export const getTvShowEpisode = async(req,res) =>{

        try{

            const { tvShowId } = req.params;

            const { seasonId } = req.params;

            const { episodeId } = req.params;

            const tvShowEpisode = await TVShow.aggregate([
 

              {"$unwind": "$seasons"
                  },

               {"$unwind": "$seasons.episodes",
  
                }, 
              
              {"$match":{

                "_id": Types.ObjectId(tvShowId),
                 "seasons._id": Types.ObjectId(seasonId),
                "seasons.episodes._id": Types.ObjectId(episodeId)

              }},

                   
           {"$lookup": {from: 'actors', localField: 'seasons.episodes.cast', foreignField: '_id', as: 'actors'}}, 

            {"$lookup": {from: 'directors', localField: 'seasons.episodes.director', foreignField: '_id', as: 'director'}},


           /*  
                //CAUSES AN ISSUE: THESE LINES OF CODE DOESN'T ALLOW to populate actors and director
           {"$project": {
                "_id": Types.ObjectId(tvShowId),
                "seasonsId": Types.ObjectId(seasonId),
                "seasonsNumber": "$seasons.number",
                "seasonsYear": "$seasons.year",
                "episode": "$seasons.episodes"
                  
                }} */

            ]);


              if (tvShowEpisode) {
                res.send(tvShowEpisode);
              } else {
                res
                  .status(404)
                  .send({ mensaje: `tv show episode not found` });
              }



        }
        catch(error){
                res.status(500).send({message: `error: \n\n${error}`});
        }


  };



  //CRUD METHODS

export const createTvShow = async (req, res) => {
  const { name, year, country, seasons } = req.body;

  try {
    const newTvShow = new TVShow({
      name,
      year,
      country,
      seasons
    });

    const tvShowSaved = await newTvShow.save();

    res.status(201).json(tvShowSaved);
  } catch (error) {
    return res.status(500).json(error);
  }
};

export const updateTvShowById = async (req, res) => {
  const updatedTvShow = await TVShow.findByIdAndUpdate(
    req.params.tvShowId,
    req.body,
    {
      new: true,
    }
  );
  res.status(200).json(updatedTvShow);
};

export const deleteTvShowById = async (req, res) => {
  const { tvShowId } = req.params;

  await TVShow.findByIdAndDelete(tvShowId);

  // code 200 is ok too
  res.status(200).json();
};