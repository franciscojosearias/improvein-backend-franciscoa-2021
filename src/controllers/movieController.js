import Movie from "../models/Movie";


//GET METHODS

  
export const getMovies = async (req, res) => {
    const movies = await Movie.find()
    .populate({path: 'director'})
    .populate({path: 'cast'});
    return res.json(movies);
  };


export const getMovieById = async (req, res) => {
    const { movieId } = req.params;
  
    const movie = await Movie.findById(movieId)
    .populate({path: 'director'})
    .populate({path: 'cast'});
    res.status(200).json(movie);
  };

export const getSpecificMoviesSortedByYear = async(req,res) =>{

    try{


      const findQuer = req.query.country ? {country: req.query.country} : {}
      
      
        const movies = await Movie.find(findQuer).sort({year: 1})
          .populate({path: 'director'})
          .populate({path: 'cast'})
        ;
      
        return res.json(movies);
      
      
        }catch (e) {
          res.status(500).send({ mensaje: e });
        }



};


  //CRUD METHODS

export const createMovie = async (req, res) => {
  const { title, year, country, genre, director, cast } = req.body;

  try {
    const newMovie = new Movie({
      title,
      year,
      country,
      genre,
      director,
      cast
    });

    const movieSaved = await newMovie.save();

    res.status(201).json(movieSaved);
  } catch (error) {
    return res.status(500).json(error);
  }
};

export const updadeMovieById = async (req, res) => {
  const updatedMovie = await Movie.findByIdAndUpdate(
    req.params.movieId,
    req.body,
    {
      new: true,
    }
  );

  res.status(200).json(updatedMovie);
};

export const deleteMovieById = async (req, res) => {
  const { movieId } = req.params;

  await Movie.findByIdAndDelete(movieId);


  res.status(200).json();
};