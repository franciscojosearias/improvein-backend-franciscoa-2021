import Actor from "../models/Actor";

//GET METHODS

export const getActors = async (req, res) => {
  const actors = await Actor.find();
  return res.json(actors);
};

export const getActorById = async (req, res) => {
  const { actorId } = req.params;

  const actor = await Actor.findById(actorId);
  res.status(200).json(actor);
};

export const getActorByNationality = async (req,res) =>{

  try{

    
    const findQuer = req.query.nationality ? {nationality: req.query.nationality} : {}

    
  const actors = await Actor.find(findQuer)
  .sort({fullName: 1});

  res.status(200).send(actors);


  }catch (e) {
    res.status(500).send({ mensaje: e });
  }
};


//CRUD METHODS 

export const createActor = async (req, res) => {
    const { fullName, birthDate, genre, nationality } = req.body;
  
    try {
      const newActor = new Actor({
        fullName,
        birthDate,
        genre,
        nationality
      });
  
      const actorSaved = await newActor.save();
  
      res.status(201).json(actorSaved);
    } catch (error) {
      return res.status(500).json(error);
    }
  };
  
  
  export const updateActorById = async (req, res) => {
    const updatedActor = await Actor.findByIdAndUpdate(
      req.params.actorId,
      req.body,
      {
        new: true,
      }
    );

    res.status(200).json(updatedActor);
  };
  
  export const deleteActorById = async (req, res) => {
    const { actorId } = req.params;
  
    await Actor.findByIdAndDelete(actorId);
    
    res.status(200).json();
  };