import { Router } from "express";
const router = Router();

import * as moviesCtrl from "../controllers/movieController";

import { authJwt } from "../middlewares";

//GET Routes

router.get("/", moviesCtrl.getMovies);

router.get("/:movieId", moviesCtrl.getMovieById);

router.get("/movieask/asking", moviesCtrl.getSpecificMoviesSortedByYear);


//CRUD Routes

router.post("/",  [authJwt.verifyToken, authJwt.isModerator], 
    moviesCtrl.createMovie
);

router.put(
    "/:movieId",
    [authJwt.verifyToken, authJwt.isModerator],
    moviesCtrl.updadeMovieById
  );

  router.delete("/:movieId",
         [authJwt.verifyToken, authJwt.isAdmin],
         moviesCtrl.deleteMovieById
  );

export default router;