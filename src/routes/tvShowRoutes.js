import { Router } from "express";
const router = Router();

import * as tvShowsCtrl from "../controllers/tvShowController";

import { authJwt } from "../middlewares";

//GET Routes

router.get("/", tvShowsCtrl.getTvShows);

router.get("/:tvShowId", tvShowsCtrl.getTvShowById);

router.get("/showid/:tvShowId/seasonid/:seasonId/episodeid/:episodeId", tvShowsCtrl.getTvShowEpisode);


//CRUD Routes

router.post("/",  [authJwt.verifyToken, authJwt.isModerator], 
    tvShowsCtrl.createTvShow
);

router.put(
    "/:tvShowId",
    [authJwt.verifyToken, authJwt.isModerator],
    tvShowsCtrl.updateTvShowById
  );

  router.delete("/:tvShowId",
         [authJwt.verifyToken, authJwt.isAdmin],
         tvShowsCtrl.deleteTvShowById
  );

export default router;