import { Router } from "express";
const router = Router();

import * as actorsCtrl from "../controllers/actorController";

import { authJwt } from "../middlewares";

//GET Routes

router.get("/", actorsCtrl.getActors);

router.get("/:actorId", actorsCtrl.getActorById);

router.get("/actorsask/ask", actorsCtrl.getActorByNationality);


//CRUD Routes

router.post("/",  [authJwt.verifyToken, authJwt.isModerator], 
    actorsCtrl.createActor
);

router.put(
    "/:actorId",
    [authJwt.verifyToken, authJwt.isModerator],
    actorsCtrl.updateActorById
  );

  router.delete("/:actorId",
         [authJwt.verifyToken, authJwt.isAdmin],
         actorsCtrl.deleteActorById
  );

export default router;