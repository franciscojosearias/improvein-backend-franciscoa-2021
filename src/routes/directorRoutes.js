import { Router } from "express";
const router = Router();

import * as directorsCtrl from "../controllers/directorController";

import { authJwt } from "../middlewares";

//GET Routes

router.get("/", directorsCtrl.getDirectors);

router.get("/:directorId", directorsCtrl.getDirectorById);

router.get("/directorsask/ask", directorsCtrl.getDirectorByNationality);


//CRUD Routes

router.post("/",  [authJwt.verifyToken, authJwt.isModerator], 
    directorsCtrl.createDirector
);

router.put(
    "/:directorId",
    [authJwt.verifyToken, authJwt.isModerator],
    directorsCtrl.updateDirectorById
  );

  router.delete("/:directorId",
         [authJwt.verifyToken, authJwt.isAdmin],
           directorsCtrl.deleteDirectorById
  );

export default router;