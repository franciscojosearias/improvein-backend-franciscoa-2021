import { Schema, model } from "mongoose";

const movieSchema = new Schema({

    title: String,

    year: Number,

    country: String,

    genre: String,

    director:{
        type: Schema.Types.ObjectId,
        ref: 'directors'
    },
    cast:[{
        type: Schema.Types.ObjectId,
        ref:'actors'
    }]


},

        {

            versionKey: false

        }


);


export default model("movies", movieSchema);