import { Schema, model } from "mongoose";

const tvShowSchema = new Schema({

    name: String,

    year: Number,

    country: String,

    seasons: [{

        number: Number,

        year: Number,

        episodes:[{
            title: String,

            number: Number,

            releasedOn: Date,

            description: String,
     
    cast:[
        {
            type: Schema.Types.ObjectId,
            ref:'actors'
          }],

    director:{
        type: Schema.Types.ObjectId,
        ref:'directors'
    }


},

{timeStamps: true
              }

            ]

    }]

});

export default model("tvshows", tvShowSchema);