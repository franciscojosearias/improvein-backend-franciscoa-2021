import { Schema, model } from "mongoose";

const directorSchema = new Schema({

     fullName: String,

     nationality: String

     }
     
     );

export default model('directors', directorSchema);