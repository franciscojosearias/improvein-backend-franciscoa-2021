import { Schema, model } from "mongoose";

const actorSchema = new Schema({

        fullName: String,

        birthDate: Date,

        genre: String,

        nationality: String

},
{
        
    timestamps:{required: true}

}


)

export default model('actors', actorSchema);